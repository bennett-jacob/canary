# Canary

The anonymous, server-renered social network that works.

## Getting started

```bash
docker-compose build
docker-compose up
```

Running a Django command:

1. Get into the shell _while the app is up_ (i.e. after running `docker-compose up`).

```bash
docker-compose exec app bash
```

2. Run all of your `python manage.py` commands.

Ref https://medium.com/zeitcode/a-simple-recipe-for-django-development-in-docker-bonus-testing-with-selenium-6a038ec19ba5
