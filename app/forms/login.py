from django.contrib.auth import authenticate
from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(
        label='Username',
        widget=forms.TextInput(
            attrs={
                'class': 'input',
                'autofocus': 'autofocus',
            }
        ),
    )

    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'input',
            }
        ),
    )

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        # authenticate in the form to use the form errors
        user = authenticate(
            username=cleaned_data.get('username'),
            password=cleaned_data.get('password'),
        )
        if user is None:
            raise forms.ValidationError(
                'The username or password was invalid.',
            )
