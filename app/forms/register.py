from django import forms
from django.contrib.auth.models import User
from django.db import IntegrityError

from ..util.username_blacklist import username_blacklist

class RegisterForm(forms.Form):
    username = forms.CharField(
        label='Username',
        widget=forms.TextInput(
            attrs={
                'class': 'input',
                'autofocus': 'autofocus',
            }
        ),
    )

    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'input',
            }
        ),
    )

    confirm_password = forms.CharField(
        label='Confirm password',
        widget=forms.PasswordInput(
            attrs={
                'class': 'input',
            }
        ),
    )

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        username = cleaned_data.get('username')
        if username in username_blacklist:
            raise forms.ValidationError(
                'That username is not allowed.',
            )

        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')

        if password != confirm_password:
            raise forms.ValidationError(
                'Password and confirmation do not match.',
            )

        # register user in form to use form validation tools
        try:
            user = User.objects.create_user(
                cleaned_data.get('username'),
                None,
                cleaned_data.get('password'),
            )
        except IntegrityError:
            raise forms.ValidationError(
                'I think that username already exists.',
            )

