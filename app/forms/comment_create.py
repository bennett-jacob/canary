from django import forms
from django.db import IntegrityError


class CommentCreateForm(forms.Form):
    body = forms.CharField(
        label='Your comment',
        widget=forms.Textarea(
            attrs={
                'class':'textarea',
                'rows':'2',
                'style':'resize:none',
            }
        ),
    )
