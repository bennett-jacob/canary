from django import forms
from django.db import IntegrityError


class PostCreateForm(forms.Form):
    title = forms.CharField(
        label='Title',
        widget=forms.TextInput(
            attrs={
                'class': 'input',
                'autofocus': 'autofocus',
            }
        ),
    )

    body_raw = forms.CharField(
        label='Content',
        widget=forms.Textarea(
            attrs={
                'class':'textarea',
                'rows':'10',
                'style':'resize:none',
            }
        ),
    )
