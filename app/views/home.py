from django.shortcuts import redirect, render

from ..models import Post, Room

def home(request):
    rooms = Room.objects.order_by('name')
    posts = Post.objects.all().prefetch_related('room').order_by('-created_at')[:20]
    context = {
        'rooms': rooms,
        'posts': posts,
    }
    return render(request, 'app/home.html', context)
