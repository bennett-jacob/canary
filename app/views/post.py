from django.shortcuts import get_object_or_404, redirect, render

from ..forms import CommentCreateForm, PostCreateForm
from ..models import Comment, Post, Room

def post_single(request, post_id):
    """Displays a post.
    """
    post = get_object_or_404(Post.objects.select_related(), pk=post_id)

    if request.method == 'POST':
        submitted_form = CommentCreateForm(request.POST)
        if submitted_form.is_valid():
            comment = Comment()

            comment.body = submitted_form.cleaned_data['body']
            comment.post = post
            comment.user = request.user if request.user.is_authenticated else None
            comment.save()

    post.view_count += 1
    post.save()

    comments = Comment.objects.filter(post=post).select_related('user').order_by('created_at')
    # comments_queryset = Comment.objects.filter(post=post).select_related('user').order_by('created_at')
    # comments = {}
    # for comment in comments_queryset:
    #     if not comment.parent:
    #         comments[comment.id] = comment
    #     else:
    #         comments[comment.parent.id].replies.apppend(comment)

    form = CommentCreateForm()

    context = {
        'post': post,
        'comments': comments,
        'form': form,
    }
    return render(request, 'app/post_single.html', context)

def post_create(request, room_slug):
    """Create a post in the specified room.
    """
    room = get_object_or_404(Room, slug=room_slug)

    if not room.is_anon_post_allowed and not request.user.is_authenticated:
        return redirect('auth_login')

    if room.is_staff_only_post and not request.user.is_staff:
        return redirect(
            'room',
            room_slug=room_slug,
        )

    if request.method == 'POST':
        form = PostCreateForm(request.POST)
        if form.is_valid():
            post = Post()

            post.title = form.cleaned_data['title']
            post.body_raw = form.cleaned_data['body_raw']
            post.room = room
            post.user = request.user if request.user.is_authenticated else None
            post.save()

            return redirect(
                'post_single',
                post_id=post.id,
            )
    else:
        form = PostCreateForm()

    return render(request, 'app/post_create.html', {
        'form': form,
    })
