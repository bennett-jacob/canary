from django.test import TestCase

from .post import Post
from .room import Room

def create_room(name, slug):
    """Create a room.
    """
    room = Room.objects.create(
        name=name,
        slug=slug,
        is_anon_post_allowed=True,
    )
    return room

def create_post(title, body, room, user=None):
    """Create a post.
    Create a post with the given `title` and `body`
    belonging to the given `user`.
    """
    post = Post.objects.create(
        title=title,
        body_raw=body,
        room=room,
        user=user if user else None,
    )
    return post

class PostViewTests(TestCase):
    def setUp(self):
        self.room = create_room(
            'PostViewTestRoom',
            'postviewtestroom',
        )

    def test_can_create_anonymous_post(self):
        post_test_title = 'Test title'
        post_test_body = 'Test body'
        post = create_post(
            post_test_title,
            post_test_body,
            self.room,
        )
        self.assertIs(post.title, post_test_title)
        self.assertIs(post.body_raw, post_test_body)
