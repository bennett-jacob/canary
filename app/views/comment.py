from django.shortcuts import get_object_or_404, redirect, render

from ..forms import CommentCreateForm
from ..models import Comment, Post, Room

def comment_reply_create(request, comment_id):
    """Creates a comment as a reply to another comment.
    """
    comment = get_object_or_404(Comment, pk=comment_id)

    if request.method == 'POST':
        form = CommentCreateForm(request.POST)
        if form.is_valid():
            new_comment = Comment()

            new_comment.body = form.cleaned_data['body']
            new_comment.post = comment.post
            new_comment.parent = comment if not comment.parent else comment.parent
            new_comment.user = request.user if request.user.is_authenticated else None
            new_comment.save()

            return redirect(
                'post_single',
                post_id=comment.post.id,
            )
    else:
        form = CommentCreateForm()

        context = {
            'comment': comment,
            'form': form,
        }

        return render(request, 'app/comment_reply_create.html', context)
