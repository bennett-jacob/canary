from django.shortcuts import get_object_or_404, render

from ..models import Post, Room

def room(request, room_slug):
    """Displays the latest posts in a room.
    """
    # user can sort posts by "latest" or "votes" by adding `?sort=<param>` to url
    sort_param = request.GET.get('sort', None)
    sort_order = '-score' if sort_param == 'votes' else '-created_at'

    room = get_object_or_404(Room, slug=room_slug)
    posts = Post.objects.filter(room__slug=room_slug).order_by(sort_order)[:10]
    context = {
        'room': room,
        'posts': posts,
    }
    return render(request, 'app/room.html', context)

def room_all(request):
    # user can sort posts by "latest" or "votes" by adding `?sort=<param>` to url
    sort_param = request.GET.get('sort', None)
    sort_order = '-score' if sort_param == 'votes' else '-created_at'

    posts = Post.objects.filter().order_by(sort_order)[:10]

    context = {
        'room': {
            'name': 'All',
            'slug': 'all',
        },
        'posts': posts,
    }
    return render(request, 'app/room.html', context)
