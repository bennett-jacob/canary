from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render

from ..forms import LoginForm, RegisterForm

def auth_register(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            return redirect('auth_login')
    else:
        form = RegisterForm()

    return render(request, 'app/auth_register.html', {
        'form': form,
    })

def auth_login(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                request,
                username=form.cleaned_data.get('username'),
                password=form.cleaned_data.get('password'),
            )
            if user is not None:
                login(request, user)
                return redirect('home')
    else:
        form = LoginForm()

    return render(request, 'app/auth_login.html', {
        'form': form,
    })

@login_required
def auth_logout(request):
    if request.method == 'POST':
        logout(request)
        return redirect('auth_login')
    return render(request, 'app/auth_logout.html')
