from django.contrib import admin

from .models import Comment, Post, Room

admin.site.register(Comment)
admin.site.register(Post)
admin.site.register(Room)
