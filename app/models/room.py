from django.core.exceptions import ValidationError
from django.db import models

disallowed_room_slugs = [
    'all',
]

class Room(models.Model):
    """Rooms are separate communities for posts.
    Think "subreddit."
    """
    name = models.CharField(max_length=32)
    slug = models.SlugField(
        max_length=32,
        unique=True,
    )
    created_at = models.DateTimeField(auto_now_add=True)

    # is_anon_post_allowed
    # If true, non-logged-in users can add a post.
    is_anon_post_allowed = models.BooleanField(default=False)

    # is_staff_only_post
    # If true, only staff members can add a post.
    is_staff_only_post = models.BooleanField(default=False)

    def __str__(self):
        return 'r/%s' % (self.slug)

    def save(self, *args, **kwargs):
        if self.slug in disallowed_room_slugs:
            raise ValidationError("That slug isn't allowed.")

        super(Post, self).save(*args, **kwargs)
