from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from mptt.models import MPTTModel, TreeForeignKey

from ..util.markdown_to_html import markdown_to_html
from ._mixins import ModelDiffMixin
from .post import Post
from .room import Room

class Comment(MPTTModel, ModelDiffMixin):
    """Comments are responses to Posts.
    """
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    parent = TreeForeignKey(
        'self',
        related_name='replies',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s: %s' % (self.id, self.body)
