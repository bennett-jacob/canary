from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from ..util.markdown_to_html import markdown_to_html
from ._mixins import ModelDiffMixin
from .room import Room

class Post(models.Model, ModelDiffMixin):
    """Posts are the core object in the app.
    """
    title = models.CharField(max_length=128)
    body_raw = models.TextField()
    body_html = models.TextField(blank=True)
    score = models.IntegerField(default=0)
    view_count = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def set_body_html_from_body_raw(self):
        self.body_html = markdown_to_html(self.body_raw)

    def __str__(self):
        return '%s: %s' % (self.id, self.title)

    def save(self, *args, **kwargs):
        if 'body_raw' in self.changed_fields:
            # `self.body_raw` was changed; update `self.body_html`.
            self.set_body_html_from_body_raw()

        if not self.room.is_anon_post_allowed and self.user is None:
            raise ValidationError("This room doesn't allow anonymous posts.")

        if self.room.is_staff_only_post and not self.user.is_staff:
            raise ValidationError("This room doesn't allow posts from non-staff.")

        super(Post, self).save(*args, **kwargs)
