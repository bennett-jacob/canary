import bleach
import mistune

from django.conf import settings

def markdown_to_html(markdown):
    dirty_html = mistune.markdown(markdown)
    clean_html = bleach.clean(
        dirty_html,
        tags=settings.BLEACH_ALLOWED_TAGS,
        attributes=settings.BLEACH_ALLOWED_ATTRIBUTES,
        protocols=settings.BLEACH_ALLOWED_PROTOCOLS,
    )

    return clean_html
