from django.conf import settings

def constants(request):
    return {
        'SITE_NAME': settings.SITE_NAME,
    }
