from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('r/all/', views.room_all, name='all'),
    path('p/<int:post_id>/', views.post_single, name='post_single'),
    path('r/<slug:room_slug>/', views.room, name='room'),
    path('r/<slug:room_slug>/new/', views.post_create, name='post_create'),
    path('c/<int:comment_id>/reply/', views.comment_reply_create, name='comment_reply_create'),
    path('auth/register/', views.auth_register, name='auth_register'),
    path('auth/login/', views.auth_login, name='auth_login'),
    path('auth/logout/', views.auth_logout, name='auth_logout'),
]
